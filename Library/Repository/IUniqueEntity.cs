namespace Lumosa.Repository
{
    public interface IUniqueEntity<TUniqueId>
    {
        TUniqueId Id { get; set;}
    }
}