using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Lumosa.Repository
{
    public interface IGenericRepository<TEntity, TUniqueId> where TEntity : IUniqueEntity<TUniqueId> 
    {
        Task<TEntity> GetById(TUniqueId id);
        Task<IEnumerable<TEntity>> GetAll();
        Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> Add(TEntity entity);
        Task<IEnumerable<TEntity>> AddRange(IEnumerable<TEntity> entities);
        Task<TEntity> Update(TEntity entity);
        Task<bool> Remove(Expression<Func<TEntity, bool>> expression);
        Task<bool> RemoveRange(IEnumerable<TEntity> entities);
        Task<bool> RemoveById(TUniqueId id);
        Task<long> Count();

        /// <summary>
        /// Return a Linq queryable to perform lambda operations in the collection.
        /// For MongoDb implementations should return Collection.AsQueryable.
        /// </summary>
        /// <returns>IQueryable</returns>
        IQueryable<TEntity> Queryable();
    }
}